require 'sinatra'
require_relative 'controllers/invoice_creator'

class App < Sinatra::Base
  configure :development do
    register Sinatra::Reloader
  end

  get '/' do
    erb :index
  end

  post '/invoices' do
    file = params[:file][:tempfile].path
    pdf = PDFCreator.new
    file_path = pdf.create_pdfs(file)
    send_file(file_path)
    erb :invoices
  end

end
