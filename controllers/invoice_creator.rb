require 'prawn'
require 'prawn/table'
require 'money'
require 'csv'
require 'zip'
require 'fileutils'

I18n.enforce_available_locales = false

class PDFCreator
  def initialize
    # Each line item is a hash that is held within an array
    @pdf = Prawn::Document.new
  end

  def create_pdfs(file)
    directory = File.dirname(file)
    headers = retrieve_headers(file)
    client_dir = nil
    invoice_folder = File.expand_path("../../public/invoices", __FILE__)
    FileUtils.rm_rf(invoice_folder) if Dir.exists?(invoice_folder)
    FileUtils.mkdir(invoice_folder)
    CSV.read(file, headers: true).each do |row|
      create_bill_to(row)
      create_remit_ach
      create_image
      create_invoice_table(headers, row)
      create_address_info
      create_send_checks
      create_line_item_table(headers, row)
      create_additional_info_box
      create_footer
      client_dir = "#{directory}/#{row[15].gsub(' ', '_')}"
      Dir.mkdir(client_dir) unless Dir.exists?(client_dir)
      render_document("#{client_dir}/#{row[20]}.pdf")
    end
    file_path = zip_directory(client_dir)
    return file_path
  end

  def zip_directory(client_directory)
    output_file = File.expand_path("../../public/invoices/#{Time.now.strftime('%Y_%m_%d_%H_%M_%S')}.zip", __FILE__)
    puts output_file
    FileUtils.rm(output_file) if File.exists?(output_file)
    Zip::File.open(output_file, Zip::File::CREATE) do |zipfile|
      Dir["#{client_directory}/*"].each do |filename|
        zipfile.add(File.basename(filename), File.join(filename))
      end
    end
    return output_file
  end

  def retrieve_headers(csv)
    csv = CSV.read(csv, headers: true)
    csv.headers
  end

  def create_image
    @pdf.image(
      File.expand_path('../../public/TaskEasyLogo.png', __FILE__),
      at: [5, 720],
      width: 155,
      height: 45
    )
  end

  def create_address_info
    @pdf.formatted_text_box(
      [
        {
          text: "669 S. West Temple, Suite 300 | Salt Lake City, UT 84101\nPhone: (801) 903-2078 | Fax: (800) 721-2380 | invoicing@taskeasy.com",
          size: 6.5
        }
      ],
      at: [10, 670],
      width: 300,
      height: 25
    )
  end

  def create_remit_ach
    @pdf.formatted_text_box(
      [
        {text: "Please remit ACH payment to:\n\n",
         size: 6.5,
         styles: [:bold]},
        {
          text: "TaskEasy, Inc.\nBank Name: Square 1 Bank\nRouting No.: 053112615\nAccount No.: 3130701",
          size: 6.5
        }
      ],
      at: [10, 570],
      width: 155,
      height: 65
    )
  end

  def create_send_checks
    @pdf.formatted_text_box(
      [
        {text: "Please send checks to:\n\n",
         size: 6.5,
         styles: [:bold]},
        {
          text: "TaskEasy, Inc.\n669 S. West Temple, Suite 300\nSalt Lake City, UT 84101",
          size: 6.5
        }
      ],
      at: [225, 570],
      width: 155,
      height: 65
    )
  end

  def create_bill_to(row)
    @pdf.formatted_text_box(
      [
        {
          text: "Bill to:\n#{row[15]}\n",
          size: 6.5,
          styles: [:bold]
        },
        {
          text: "#{row[16]}\n#{row[17]}, #{row[18]} #{row[19]}",
          size: 6.5
        }
      ],
      at: [10, 630],
      width: 165,
      height: 55
    )

  end

  def create_additional_info_box
    @pdf.move_cursor_to(85)
    @pdf.table(
      [['Additional Information']],
      position: :center,
      column_widths: [540]
    ) do |table|
      table.cells.style(
        text_color: 'FFFFFF',
        background_color: '91be3e',
        font_style: :bold,
        size: 6.5
      )
    end
  end

  def create_footer
    @pdf.formatted_text_box(
      [
        {
          text: "A 1.5% Fee will be accessed to balances paid after the due date.\nIf you have questions regarding this invoice, please e-mail invoicing@taskeasy.com \nLearn more about our commitment to privacy at http://www.taskeasy.com/privacy\nRead our Conditions of Use at www.taskeasy.com/terms",
          size: 6.5,
          padding: 10
        }
      ], at: [5, 55],
      height: 40,
      width: 540
    )
  end

  def render_document(file_location)
    @pdf.render_file(file_location)
  end

  def create_line_item_table(headers, row)
    headers = headers[0..12]
    empty_lines = []
    13.times {empty_lines << ' '}
    data = [headers, row[0..12], empty_lines]
    18.times {data << empty_lines}
    @pdf.move_down 150
    @pdf.table(
      data,
      header: true,
      position: :left,
      row_colors: ['FFFFFF', 'D3D3D3']
    ) do |table|
      table.header = headers
      table.cells.row(0).style(text_color: 'FFFFFF', background_color: '91be3e', font_style: :bold, size: 6.5)
      table.cells.row(1..25).style(size: 6.5)
      table.rows(1..25).style(single_line: false)
    end
  end

  def create_invoice_table(headers, row)
    @pdf.move_cursor_to(720)
    data = [
      [{content: 'Invoice', colspan: 2}],
      [headers[20], row[20]],
      [headers[20], row[20]],
      [headers[21], row[21]],
      [headers[22], row[22]]
    ]
    @pdf.table(
      data,
      position: :right,
      width: 150
    ) do |table|
      table.cells.row(0).style(text_color: 'FFFFFF', background_color: '91be3e', font_style: :bold)
      table.cells.row(1..4).style(background_color: 'FFFFFF', size: 6.5)
      table.cells.row(0).style(size: 6.5)
      table.rows(1..25).style(single_line: false)
    end
  end

end
